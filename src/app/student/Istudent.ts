export interface IStudent {
  id: number;
  fullname: string;
  gender: string;
  address: string;
  phoneNumber: number;
}
