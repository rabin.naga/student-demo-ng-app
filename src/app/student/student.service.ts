import { IStudent } from "./Istudent";

import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class StudentService {
  private _url = "http://localhost:3000/student";
  constructor(private http: HttpClient) {}
  getStudents(): Observable<IStudent[]> {
    return this.http.get<IStudent[]>(this._url);
  }

  getStudent(id: number): Observable<IStudent> {
    return this.http.get<IStudent>(`${this._url}/${id}`);
  }

  addStudent(student: IStudent): Observable<IStudent> {
    return this.http.post<IStudent>(this._url, student, {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    });
  }

  updateStudent(student: IStudent): Observable<void> {
    return this.http.put<void>(`${this._url}/${student.id}`, student, {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    });
  }

  deleteStudent(id: number): Observable<void> {
    return this.http.delete<void>(`${this._url}/${id}`);
  }
}
