import { IStudent } from "./Istudent";
import { StudentService } from "./student.service";
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-create-student",
  templateUrl: "./create-student.component.html",
  styleUrls: ["./create-student.component.scss"]
})
export class CreateStudentComponent implements OnInit {
  studentForm: FormGroup;
  student: IStudent;
  pageTitle: string;
  // object of validation msgs that are dynamically added to form controls
  validationMessages = {
    fullName: {
      required: "full name is required",
      minlength: "full name must be greater than 2 characters",
      maxlength: "full name must be less than 10 characters"
    },
    gender: {
      required: "email is required"
    },
    address: {
      required: "address is required"
    },
    phoneNumber: {
      required: "phone is required"
    }
  };
  // object for saving error message when validation fails
  formErrors = {
    fullName: "",
    gender: "",
    address: "",
    phoneNumber: ""
  };

  logValidationErrors(group: FormGroup = this.studentForm): void {
    // Loop through each control key in the FormGroup
    Object.keys(group.controls).forEach((key: string) => {
      // Get the control. The control can be a nested form group
      const abstractControl = group.get(key);
      // If the control is nested form group, recursively call
      // this same method
      if (abstractControl instanceof FormGroup) {
        this.logValidationErrors(abstractControl);
        // If the control is a FormControl
      } else {
        // Clear the existing validation errors
        this.formErrors[key] = "";
        if (
          abstractControl &&
          !abstractControl.valid &&
          (abstractControl.touched || abstractControl.dirty)
        ) {
          // Get all the validation messages of the form control
          // that has failed the validation
          const messages = this.validationMessages[key];
          // Find which validation has failed. For example required,
          // minlength or maxlength. Store that error message in the
          // formErrors object. The UI will bind to this object to
          // display the validation errors
          for (const errorKey in abstractControl.errors) {
            if (errorKey) {
              this.formErrors[key] += messages[errorKey] + " ";
            }
          }
        }
      }
    });
  }

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private studentService: StudentService,
    private router: Router
  ) {}

  ngOnInit() {
    this.buildForm();
    this.loadForm();
    // this code is loads the logvalidationErrors method when form value changes
    this.studentForm.valueChanges.subscribe(data => {
      this.logValidationErrors(this.studentForm);
    });
  }

  buildForm() {
    this.studentForm = this.fb.group({
      fullName: [
        "",
        Validators.required
        // Validators.minLength(2),
        // Validators.maxLength(10)
      ],
      gender: [""],
      address: ["", Validators.required],
      phoneNumber: ["", Validators.required]
    });
  }

  loadForm() {
    this.route.paramMap.subscribe(params => {
      const studentId = +params.get("id");
      if (studentId) {
        this.pageTitle = "Edit student record";
        this.getStudent(studentId);
      } else {
        this.pageTitle = "Create student record";

        this.student = {
          id: null,
          fullname: "",
          gender: "",
          address: "",
          phoneNumber: null
        };
      }
    });
  }
  getStudent(id: number) {
    this.studentService.getStudent(id).subscribe(
      (student: IStudent) => {
        this.editStudent(student);
        this.student = student;
      },
      (err: any) => console.log(err)
    );
  }

  editStudent(student: IStudent) {
    console.log(student);
    this.studentForm.patchValue({
      fullName: student.fullname,
      gender: student.gender,
      address: student.address,
      phoneNumber: student.phoneNumber
    });
  }

  onSubmit() {
    this.mapFormValuestoStdentModel();
    console.log(this.studentForm.value);

    if (this.student.id) {
      this.studentService
        .updateStudent(this.student)
        .subscribe(
          () => this.router.navigate(["student"]),
          err => console.log(err)
        );
    } else {
      this.studentService
        .addStudent(this.student)
        .subscribe(
          () => this.router.navigate(["student"]),
          err => console.log(err)
        );
    }
  }
  mapFormValuestoStdentModel() {
    this.student.fullname = this.studentForm.value.fullName;
    this.student.gender = this.studentForm.value.gender;
    this.student.address = this.studentForm.value.address;
    this.student.phoneNumber = this.studentForm.value.phoneNumber;
  }
}
