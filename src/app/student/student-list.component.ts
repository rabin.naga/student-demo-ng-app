import { IStudent } from "./Istudent";
import { Component, OnInit } from "@angular/core";
import { StudentService } from "./student.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-student-list",
  templateUrl: "./student-list.component.html",
  styleUrls: ["./student-list.component.css"]
})
export class StudentListComponent implements OnInit {
  students: IStudent[];
  student: IStudent;
  constructor(
    private _studentService: StudentService,
    private _router: Router
  ) {}

  ngOnInit() {
    this._studentService
      .getStudents()
      .subscribe(
        listStudent => (this.students = listStudent),
        err => console.log(err)
      );
  }

  editBtnClick(studentId: number) {
    this._router.navigate(["/edit", studentId]);
  }

  deleteBtnClick(studentId: number) {
    this._studentService
      .deleteStudent(studentId)
      .subscribe(() => this._router.navigate(["/student"]));
  }
}
