import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { StudentRoutingModule } from "./student-routing.module";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";

import { StudentListComponent } from "./student-list.component";
import { CreateStudentComponent } from "./create-student.component";
import { from } from "rxjs";

@NgModule({
  declarations: [StudentListComponent, CreateStudentComponent],
  imports: [
    CommonModule,
    StudentRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class StudentModule {}
