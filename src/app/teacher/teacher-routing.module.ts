import { TeacherCreateComponent } from "./teacher-create.component";
import { TeacherListComponent } from "./teacher-list.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  { path: "", component: TeacherListComponent },
  { path: "create", component: TeacherCreateComponent },
  { path: "edit/:id", component: TeacherCreateComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeacherRoutingModule {}
