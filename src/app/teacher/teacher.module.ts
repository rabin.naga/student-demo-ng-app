import { TeacherRoutingModule } from "./teacher-routing.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { NgxPaginationModule } from "ngx-pagination";
import { OrderModule } from "ngx-order-pipe";
import { FilterPipeModule } from "ngx-filter-pipe";
import { Ng2SearchPipeModule } from "ng2-search-filter";
// import { teacherFilterPipe } from "./teacher-filter.pipe";
import { teacherFilterPipe } from "./teacher-filter.pipe";

import { TeacherCreateComponent } from "./teacher-create.component";
import { TeacherListComponent } from "./teacher-list.component";
import { DxButtonModule } from "devextreme-angular";
import { DxDataGridModule } from "devextreme-angular";
@NgModule({
  declarations: [
    TeacherCreateComponent,
    TeacherListComponent,
    teacherFilterPipe
  ],
  imports: [
    CommonModule,
    TeacherRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,
    OrderModule,
    FilterPipeModule,
    Ng2SearchPipeModule,
    DxButtonModule,
    DxDataGridModule
  ]
})
export class TeacherModule {}
