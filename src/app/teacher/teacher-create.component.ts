import { ActivatedRoute, Router } from "@angular/router";
import { TeacherService } from "./teacher.service";
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from "@angular/forms";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-teacher-create",
  templateUrl: "./teacher-create.component.html",
  styleUrls: ["./teacher-create.component.scss"]
})
export class TeacherCreateComponent implements OnInit {
  teacherForm: FormGroup;
  pageTitile: string;
  teacherId: number;
  formSubmit: string;
  // object of validation msgs that are dynamically added to form controls
  validationMessages = {
    fullName: {
      required: "full name is required",
      minlength: "full name must be greater than 2 characters",
      maxlength: "full name must be less than 10 characters"
    },
    gender: {
      required: "email is required"
    },
    address: {
      required: "address is required"
    },
    phoneNumber: {
      required: "phone is required",
      pattern: "pattern is invalid"
    }
  };
  // object for saving error message when validation fails
  formErrors = {
    fullName: "",
    gender: "",
    address: "",
    phoneNumber: ""
  };
  logValidationErrors(group: FormGroup = this.teacherForm): void {
    // Loop through each control key in the FormGroup
    Object.keys(group.controls).forEach((key: string) => {
      // Get the control. The control can be a nested form group
      const abstractControl = group.get(key);
      // If the control is nested form group, recursively call
      // this same method
      if (abstractControl instanceof FormGroup) {
        this.logValidationErrors(abstractControl);
        // If the control is a FormControl
      } else {
        // Clear the existing validation errors
        this.formErrors[key] = "";
        if (
          abstractControl &&
          !abstractControl.valid &&
          (abstractControl.touched || abstractControl.dirty)
        ) {
          // Get all the validation messages of the form control
          // that has failed the validation
          const messages = this.validationMessages[key];
          console.log(messages);
          console.log(abstractControl.errors);
          // Find which validation has failed. For example required,
          // minlength or maxlength. Store that error message in the
          // formErrors object. The UI will bind to this object to
          // display the validation errors
          for (const errorKey in abstractControl.errors) {
            if (errorKey) {
              this.formErrors[key] += messages[errorKey] + " ";
            }
          }
        }
      }
    });
  }

  constructor(
    private fb: FormBuilder,
    private _teacherService: TeacherService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    // console.log("this is ngONInit");
    this.buildTeacherForm();
    this.loadForm();
    // this code is loads the logvalidationErrors method when form value changes
    this.teacherForm.valueChanges.subscribe(data => {
      this.logValidationErrors(this.teacherForm);
    });
  }

  buildTeacherForm(): void {
    this.teacherForm = this.fb.group({
      fullName: [
        "",
        [Validators.required, Validators.minLength(3), Validators.maxLength(10)]
      ],
      gender: [""],
      address: ["", Validators.required],
      phoneNumber: [
        "",
        [
          Validators.required,
          Validators.pattern("^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-s./0-9]*$")
        ]
      ]
    });
  }
  // gets id from route of selected item to edit and loads form accordingly..
  loadForm() {
    this.route.paramMap.subscribe(params => {
      this.teacherId = +params.get("id");
      // console.log(this.teacherId);
      if (this.teacherId) {
        this.pageTitile = "Edit teaacher record";
        this.formSubmit = "Edit";
        this.editTeacher(this.teacherId);
      } else {
        this.pageTitile = "Create teacher record";
        this.formSubmit = "Submit";
        this.buildTeacherForm();
      }
    });
  }
  // sets detail of  selected teacher into form
  editTeacher(id: number) {
    this.teacherForm.patchValue({
      fullName: this._teacherService.teacherDetail[id - 1].fullName,
      gender: this._teacherService.teacherDetail[id - 1].gender,
      address: this._teacherService.teacherDetail[id - 1].address,
      phoneNumber: this._teacherService.teacherDetail[id - 1].phoneNumber
    });
  }

  onSubmit() {
    // console.log(this.teacherForm.value);
    this.logValidationErrors(this.teacherForm);
    // console.log(this.formErrors);

    if (this.teacherId) {
      console.log("update code goes here");
      // array method to edit(replaces 1 item to the supplied array position)
      this._teacherService.teacherDetail.splice(
        this.teacherId - 1,
        1,
        this.teacherForm.value
      );
      this.router.navigate(["teacher"]);
    } else {
      this._teacherService.addTeacherDetail(this.teacherForm.value);
      this.router.navigate(["teacher"]);
    }
  }
}
