import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class TeacherService {
  teacherDetail = [
    {
      id: "1",
      fullName: "suraj",
      gender: "female",
      address: "d",
      phoneNumber: "9779876676511"
    },
    {
      id: "2",
      fullName: "ram",
      gender: "female",
      address: "c",
      phoneNumber: "9779876676522"
    },
    {
      id: "3",
      fullName: "hari",
      gender: "male",
      address: "b",
      phoneNumber: "9779876676333"
    },
    {
      id: "4",
      fullName: "rabindai",
      gender: "male",
      address: "e",
      phoneNumber: "9779876676666"
    }
  ];

  constructor() {}

  addTeacherDetail(teacherObject: any) {
    this.teacherDetail.push(teacherObject);
  }
}
