import { TeacherService } from "./teacher.service";
import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { OrderPipe } from "ngx-order-pipe";

@Component({
  selector: "app-teacher-list",
  templateUrl: "./teacher-list.component.html",
  styleUrls: ["./teacher-list.component.scss"]
})
export class TeacherListComponent implements OnInit {
  constructor(
    private _teacherService: TeacherService,
    private _router: Router,
    private route: ActivatedRoute,
    private orderPipe: OrderPipe
  ) {}
  teacherDetail = this._teacherService.teacherDetail;

  p: number = 1;

  globalSearchTerm: any;
  nameSearchTerm: any;
  genderSearchTerm: any;
  addressSearchTerm: any;
  phoneSearchTerm: any;
  fullName: string;
  gender: string;
  address: string;
  phoneNumber: string;

  id: number;

  ngOnInit() {}

  editBtnClick(teacherId: number) {
    this._router.navigate(["teacher/edit", teacherId]);
    // this._router.navigate([teacherId], { relativeTo: this.route });
    console.log(this.teacherDetail);
  }

  deleteBtnClick(i: number) {
    console.log(i);
    this._teacherService.teacherDetail.splice(i, 1);
  }
  sort(key: string) {
    this.teacherDetail = this.orderPipe.transform(this.teacherDetail, key);
  }
}
